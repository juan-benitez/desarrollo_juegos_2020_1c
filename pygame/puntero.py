import pygame, random, sys, time, math
from pygame.locals import *

WHITE = (255, 255, 255)
RED = (255, 0, 0)


class Cross(pygame.sprite.Sprite):
    def __init__(self):
        # self.image = image
        self.surface = pygame.Surface((15, 15))
        self.surface.fill(WHITE)
        pygame.draw.line(self.surface, RED, (7, 0), (7, 14), 2)
        pygame.draw.line(self.surface, RED, (0, 7), (14, 7), 2)
        self.rect = self.surface.get_rect(center = (160, 320))
        self.destination_x = 0
        self.destination_y = 0

    def move_to(self, x, y):
        self.destination_x = x #+ self.rect.left
        self.destination_y = y #+ self.rect.top
        

    def move(self):
        # self.rect.move_ip(self.destination_x, self.destination_y)
        self.rect.x = self.destination_x - 7
        self.rect.y = self.destination_y - 7


class Missile(pygame.sprite.Sprite):
    def __init__(self, image, origin, target):
        super().__init__()
        self.image = image
        self.surface = pygame.Surface((30, 15))
        self.origin = origin
        self.target = target
        self.current_pos = origin
        self.speed = 10
        self.rect = self.surface.get_rect(center = (300, 400))
        
        dx = (self.target[0] - self.origin[0])
        dy = (self.target[1] - self.origin[1])

        if dx != 0:
            ang = math.atan(dy/dx)
            self.speed_x = -self.speed * math.cos(ang)
            self.speed_y = -self.speed * math.sin(ang)
        else:
            self.speed_x = 0
            self.speed_y = self.speed

        print(self.speed_x, self.speed_y)

    def move(self):
        self.rect.x += self.speed_x
        self.rect.y += self.speed_y
        
        
    
pygame.init()

# Inicializacion
fps = 60
framePerSec = pygame.time.Clock()  

display_surface = pygame.display.set_mode((600, 400))
display_surface.fill(WHITE)
pygame.display.set_caption("Mira")

missile_image = pygame.image.load("sprites\missile.png")

cross = Cross()
missiles = pygame.sprite.Group()


inGame = True
while inGame:

    # Manejo de eventos
    for event in pygame.event.get():
        if event.type == QUIT:
            inGame = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            (mouse_x, mouse_y) = pygame.mouse.get_pos()
            cross.move_to(mouse_x, mouse_y)
            
            if pygame.mouse.get_pressed()[2]:  # Right button pressed
                # Missile fired
                missiles.add(Missile(missile_image, (300, 400),  (mouse_x, mouse_y) ))
  

    display_surface.fill(WHITE)


    for missile in missiles:
        display_surface.blit(missile.image, missile.rect)
        missile.move()

    display_surface.blit(cross.surface, cross.rect)
    cross.move()

    pygame.display.update()

    framePerSec.tick(fps)
