import pygame, random, sys, time
from pygame.locals import *


WHITE = (255, 255, 255)
RED = (255, 0, 0)

pygame.init()

# Inicializacion
fps = 60
framePerSec = pygame.time.Clock()  

display_surface = pygame.display.set_mode((400, 600))
display_surface.fill(WHITE)
pygame.display.set_caption("Rage Road")

enemy_speed = 5

# Carga de assets
background = pygame.image.load("road.png")
enemy_image = pygame.image.load("Enemy.png")
player_image = pygame.image.load("Player.png")

# Entidades (Clases)
class Player(pygame.sprite.Sprite):
    rect = None
    
    def __init__(self, player_image):
        super().__init__()
        self.image = player_image
        self.surface = pygame.Surface((44, 96))
        self.rect = self.surface.get_rect(center = (160, 520))
        self.speed = 0

    def move(self):

        if self.rect.left+self.speed > 0 and self.rect.right+self.speed < 400:
            self.rect.move_ip(self.speed, 0)


class Enemy(pygame.sprite.Sprite):
    def __init__(self, enemy_image):
        super().__init__()
        self.image = enemy_image
        self.surface = pygame.Surface((48, 93))
        pos_x = random.randint(40, 400-40)
        self.rect = self.surface.get_rect(center = (pos_x, 0))

    def move(self):
        self.rect.move_ip(0, enemy_speed)
        if self.rect.top > 600:
            self.rect.top = 0
            self.rect.center = (random.randint(40, 400-40), 0)


def spritecollidepoint(rect, x, y):
    if rect.top > y or rect.top + rect.height < y :
        return False
    
    if rect.left > x or rect.left + rect.width < x :
        return False

    return True


player = Player(player_image)
enemy = Enemy(enemy_image)

sprites = pygame.sprite.Group()
sprites.add(player)
sprites.add(enemy)

enemies = pygame.sprite.Group()
enemies.add(enemy)

inGame = True
while inGame:

    # Manejo de eventos
    for event in pygame.event.get():
        if event.type == QUIT:
            inGame = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            (mouse_x, mouse_y) = pygame.mouse.get_pos()
            pointer = pygame.Rect(mouse_x, mouse_y, 1, 1)
            
            if spritecollidepoint(player.rect, mouse_x, mouse_y):
                dx = mouse_x - player.rect.center[0]
                player.speed = dx

        elif event.type == pygame.MOUSEBUTTONUP:
            player.speed = 0


    # Dibujado
    display_surface.blit(background, (0, 0))

    for sprite in sprites:
        display_surface.blit(sprite.image, sprite.rect)
        sprite.move()

    # pygame.draw.circle(display_surface, RED, (160, 520), 4)


    # Colisiones
    # if pygame.sprite.spritecollideany(player, enemies):
    #     display_surface.fill(RED)
    #     pygame.display.update()
    #     time.sleep(2)
    #     pygame.quit()
    #     sys.exit()

    pygame.display.update()

    framePerSec.tick(fps)