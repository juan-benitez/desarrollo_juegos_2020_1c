import pygame, random, sys, time
from pygame.locals import *

# pillow   (ex pil)

HEIGHT = 400
WIDTH = 600

WHITE = (255, 255, 255)
RED = (255, 0, 0)
BLACK = (0, 0, 0)

pygame.init()

# Inicializacion
fps = 60
framePerSec = pygame.time.Clock()

ACC = 1.75
FRIC = -0.12

display_surface = pygame.display.set_mode((WIDTH, HEIGHT))
display_surface.fill(BLACK)
pygame.display.set_caption("Plataforma 1")

font = pygame.font.SysFont("Verdana", 40)
titulo = font.render("Plataformero", True, RED)


class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.surf = pygame.Surface((30, 50))
        self.surf.fill((0, 255, 255))
        self.rect = self.surf.get_rect(center=(30, 350))

        self.pos = pygame.math.Vector2((10, 385))
        self.vel = pygame.math.Vector2((0, 0))
        self.acc = pygame.math.Vector2((0, 0))

    def move(self):
        self.acc = pygame.math.Vector2((0, 0.5))

        teclas = pygame.key.get_pressed()
        if teclas[K_LEFT]:
            self.acc.x = -ACC
        if teclas[K_RIGHT]:
            self.acc.x = ACC

        self.acc.x += self.vel.x * FRIC
        self.vel += self.acc
        self.pos += self.vel + 0.5 * self.acc

        if self.pos.x > WIDTH:
            self.pos.x = 0
        if self.pos.x < 0:
            self.pos.x = WIDTH

        self.rect.midbottom = self.pos

    def jump(self):
        hits = pygame.sprite.spritecollide(self, plataformas, False)
        if hits:    # Para evitar mas de un salto
            self.vel.y = -15

    def update(self):
        hits = pygame.sprite.spritecollide(self, plataformas, False)
        if self.vel.y > 0:
            if hits:
                if self.pos.y < hits[0].rect.bottom: 
                    self.vel.y = 0
                    self.pos.y = hits[0].rect.top + 1


    
class PlataformaPiso(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.surf = pygame.Surface((WIDTH, 15))
        self.surf.fill((255, 0, 0))
        self.rect = self.surf.get_rect(center=(WIDTH/2, HEIGHT-15))

    def move(self):
        pass

class Plataforma(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.surf = pygame.Surface((random.randint(50, 120), 15))
        self.surf.fill((0, 255, 120))
        self.rect = self.surf.get_rect(center=(random.randint(0,WIDTH-50), random.randint(0, HEIGHT-30) ))

    def move(self):
        pass
    

player = Player()
piso = PlataformaPiso()

plataformas = pygame.sprite.Group()
plataformas.add(piso)
for p in range(4):
    plataformas.add(Plataforma())

sprites = pygame.sprite.Group()
sprites.add(player)
sprites.add(plataformas)

inGame = True
while inGame:

    # Manejo de eventos
    for event in pygame.event.get():
        if event.type == QUIT:
            inGame = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                player.jump()

    display_surface.fill(BLACK)
    player.update()
    
    for sprite in sprites:
        display_surface.blit(sprite.surf, sprite.rect)
        sprite.move()

    display_surface.blit(titulo, (200, 10))

    pygame.display.update()
    framePerSec.tick(fps)
