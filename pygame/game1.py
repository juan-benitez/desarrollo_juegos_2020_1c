import pygame
from pygame.locals import *

pygame.init()

fps = 30
framePerSec = pygame.time.Clock()     # Clock framePerSec = new pygame.time.Clock() 

WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)

pygame.display.set_caption('Master Game 1')

# surface
display_surface = pygame.display.set_mode((300, 450))
display_surface.fill(GREEN)

# []  Arrays
# {}  Dictionaries
# ()  Tuples

pygame.draw.line(display_surface, BLUE, (100, 400), (200, 400), 5)            # line, circle, rect


inGame = True
while inGame:

    pygame.display.update()

    for event in pygame.event.get():
        if event.type == QUIT:
            inGame = False

    framePerSec.tick(fps)
