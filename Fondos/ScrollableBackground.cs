﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fondos
{
    public class ScrollableBackground : GameComponent
    {
        private Texture2D texture;
        private Vector2 positionA, positionB;
        public float Speed { get; set; }

        public ScrollableBackground(Game game, Texture2D texture) : base(game)
        {
            this.texture = texture;
            this.positionA = Vector2.Zero;
            this.positionB = Vector2.Zero;
            this.Speed = 0;
        }

        public override void Update(GameTime gameTime)
        {
            positionA.X = positionA.X - Speed;
            if (positionA.X <= -texture.Width)
                positionA.X = 0;

            positionB.X = positionA.X + texture.Width;

            base.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, positionA, null, Color.White, 0f, Vector2.Zero, new Vector2(1, 1), SpriteEffects.None, 0);

            spriteBatch.Draw(texture, positionB, null, Color.White, 0f, Vector2.Zero, new Vector2(1, 1), SpriteEffects.None, 0);
        }
    }
}
