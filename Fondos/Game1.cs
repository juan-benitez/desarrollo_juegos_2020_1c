﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Fondos
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private ScrollableBackground background;
        private Rectangle rect;

        private ComponenteAnimado gatoA;
        private Vector2 gatoAPosition;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            background = new ScrollableBackground(this, Content.Load<Texture2D>("background"));

            var gatoAFrame = Content.Load<Texture2D>("cat_walking");

            var y = gatoAFrame.Height / 12;
            y = GraphicsDevice.Viewport.Height - y - 40;

            gatoAPosition = new Vector2(0, y);
            gatoA = new ComponenteAnimado(this, gatoAFrame, 12, gatoAPosition);

            rect = new Rectangle(0, 0, GraphicsDevice.Viewport.Width + 500, GraphicsDevice.Viewport.Height);
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                // gatoA.AvanzarX();
                gatoA.Animar();
                background.Speed = 4;
                background.Update(gameTime);
            }

            gatoA.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);

            spriteBatch.Begin();

            background.Draw(spriteBatch);
            gatoA.Draw(spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
