﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fondos
{
    // TODO: Determinar la direccion es de los cuadros (horizontal o vertical)

    public class ComponenteAnimado : GameComponent
    {
        private Texture2D frame;
        private int totalFrames;
        private int currentFrame;
        private int timer, ms;

        private Vector2 position, lastPosition;
        private Rectangle rectangle;

        private bool isAnimating = false;

        public ComponenteAnimado(Game game) : base(game)
        {
        }

        public ComponenteAnimado(Game game, Texture2D frame, int totalFrames, Vector2 position, int currentFrame = 0) : this(game)
        {
            this.frame = frame;
            this.totalFrames = totalFrames;
            this.currentFrame = currentFrame;
            this.position = position;
            this.lastPosition = position;
            this.ms = 100;
            this.rectangle = new Rectangle(0, 0, this.frame.Width, this.frame.Height / this.totalFrames);
        }

        public void Animar()
        {
            this.isAnimating = true;
        }

        public void AvanzarX(int points = 2)
        {
            this.position = new Vector2(this.position.X + points, this.position.Y);
        }

        public void AvanzarY(int points = 2)
        {
            this.position = new Vector2(this.position.X, this.position.Y + points);
        }

        public override void Update(GameTime gameTime)
        {
            this.timer += gameTime.ElapsedGameTime.Milliseconds;
            if (ms < timer && (this.position != this.lastPosition || this.isAnimating))
            {
                this.timer = 0;
                this.currentFrame = (currentFrame + 1) % this.totalFrames;
                this.rectangle.Y = (this.frame.Height / this.totalFrames) * this.currentFrame;

                this.lastPosition = this.position;
                this.isAnimating = false;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.frame, this.position, this.rectangle, Color.White, 0f, Vector2.Zero, 1, SpriteEffects.FlipHorizontally, 0);
        }
    }
}
