import pygame
from pygame.locals import *
from pygame.sprite import Group
from pygame.surface import Surface

from config import *
from platforms import LevelPlatform, FloorPlatform, Platform
from player import Player
from shot import Shot


class Game():

    def clear_screen(self):
        self.display_surface.blit(self.background_image, (0, 0))


    def __init__(self):
        pygame.init()

        # Clock settings
        self.framePerSec = pygame.time.Clock()

        # Load background image
        self.background_image: Surface = pygame.transform.scale(pygame.image.load(r'img/commonsprites/fondo.png'), (WIDTH, HEIGHT))
        self.game_over_image: Surface = pygame.transform.scale(pygame.image.load(r'img/commonsprites/gameover.png'), (WIDTH, HEIGHT))


        # Setup display surface
        self.display_surface = pygame.display.set_mode((WIDTH, HEIGHT))
        self.clear_screen()
        pygame.display.set_caption("Towel fall")

        self.floor = FloorPlatform()

        self.platforms: Group = pygame.sprite.Group()
        self.platforms.add(self.floor)
        self.platforms.add(LevelPlatform(400, (310, 155), True))

        self.platforms.add(LevelPlatform(48, (24, 234), True))
        self.platforms.add(LevelPlatform(48, (WIDTH - 24, 234), True))

        self.platforms.add(LevelPlatform(76, (38, 315), True))
        self.platforms.add(LevelPlatform(76, (WIDTH - 38, 315), True))

        self.level_sprites: Group = pygame.sprite.Group()
        self.level_sprites.add(self.platforms)

        p1_controls: dict = {
            'move_left': K_a,
            'move_right': K_d,
            'jump': K_w,
            'shoot': K_g
        }
        p1_start_position = (Player.CHARACTER_WIDTH, HEIGHT - self.floor.PLATFORM_HEIGHT)
        self.player = Player(self, "adventurer-girl", p1_controls, p1_start_position, 'right')

        p2_controls: dict = {
            'move_left': K_LEFT,
            'move_right': K_RIGHT,
            'jump': K_UP,
            'shoot': K_BACKSPACE
        }
        p2_start_position = (WIDTH - Player.CHARACTER_WIDTH, HEIGHT - self.floor.PLATFORM_HEIGHT)
        self.player2 = Player(self, "adventurer-girl", p2_controls, p2_start_position, 'left')

        self.player_sprites: Group = pygame.sprite.Group()
        self.player_sprites.add(self.player)
        self.player_sprites.add(self.player2)

        self.shot_sprites: Group = pygame.sprite.Group()

        self.in_game = True



    def start(self):
        while self.in_game:

            # Manejo de eventos
            for event in pygame.event.get():
                if event.type == QUIT:
                    self.in_game = False

            keys = pygame.key.get_pressed()

            if len(self.player_sprites) > 1:
                self.clear_screen()
                self.player_sprites.update()
                self.player_sprites.draw(self.display_surface)

                if not len(self.shot_sprites) == 0:
                    self.shot_sprites.update()

                self.shot_sprites.draw(self.display_surface)

                player_sprite: Player
                for player_sprite in self.player_sprites:
                    player_sprite.move(keys)

                shot_sprite: Shot
                for shot_sprite in self.shot_sprites:
                    shot_sprite.move()

                platform_sprite: Platform
                for platform_sprite in self.level_sprites:
                    self.display_surface.blit(platform_sprite.surf, platform_sprite.rect)
                    platform_sprite.move()

                ## Load Lifes1
                font = pygame.font.SysFont("Verdana", 20)
                titulo = font.render(f" LIFE P2: {self.player.health}", True, WHITE)
                self.display_surface.blit(titulo, (10, 370))

                ## Load Lifes2
                font = pygame.font.SysFont("Verdana", 20)
                titulo = font.render(f"LIFE P2:{self.player2.health}", True, WHITE)
                self.display_surface.blit(titulo, (500, 370))


            else:
                self.display_surface.blit(self.game_over_image, (0, 0))

            pygame.display.update()
            self.framePerSec.tick(FPS)


    @staticmethod
    def player_hit(player: Player, damage: int):
        player.take_damage(damage)

    def end_game(self):
        print("We are in the end game")
        # self.in_game = False


