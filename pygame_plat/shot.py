import glob

import pygame
from pygame.math import Vector2

from config import *


class Shot(pygame.sprite.Sprite):
    SHOT_WIDTH = 4
    SHOT_HEIGHT = 5
    SHOT_SIZE = (SHOT_WIDTH, SHOT_HEIGHT)

    SHOT_SPEED = 5
    SHOT_DAMAGE = 1

    controls: dict

    def __init__(self, game, ammo_type, starting_position: Vector2, orientation: str):
        super().__init__()
        self.game = game
        self.surf = pygame.Surface(self.SHOT_SIZE)
        self.rect = self.surf.get_rect(center=starting_position)

        self.images = {
            'Flying': [pygame.transform.scale(pygame.image.load(image), self.SHOT_SIZE)
                       for image in glob.glob(f'img\\ammo\\{ammo_type}\\Flying*.png')],
            'Impact': [pygame.transform.scale(pygame.image.load(image), self.SHOT_SIZE)
                       for image in glob.glob(f'img\\ammo\\{ammo_type}\\Impact*.png')]
        }

        self.action = 'Flying'
        self.orientation = orientation
        self.index = 0
        self.image = self.images[self.action][self.index]

        self.pos = pygame.math.Vector2(starting_position)
        if self.orientation == 'right':
            self.vel = pygame.math.Vector2((self.SHOT_SPEED, GRAVITY/10))
        else:
            self.vel = pygame.math.Vector2((-self.SHOT_SPEED, GRAVITY/10))
        # self.acc = pygame.math.Vector2((0, 0))

    def move(self):
        # self.acc = pygame.math.Vector2((0, GRAVITY))

        # if self.orientation == 'right':
        #     self.acc.x = ACC
        # else:
        #     self.acc.x = -ACC

        # self.acc.x += self.vel.x
        # self.vel += self.acc
        self.pos += self.vel  # + 1 * self.acc

        if self.pos.x > WIDTH:
            self.pos.x = 0
        if self.pos.x < 0:
            self.pos.x = WIDTH

        self.rect.midbottom = self.pos

    def update(self):
        hits = pygame.sprite.spritecollide(self, self.game.platforms, False)
        if hits:
            self.game.shot_sprites.remove(self)

        player_hits = pygame.sprite.spritecollide(self, self.game.player_sprites, False)
        if player_hits:
            self.game.shot_sprites.remove(self)
            for player in player_hits:
                self.game.player_hit(player, self.SHOT_DAMAGE)

        if self.index >= len(self.images[self.action]):
            self.index = 0

        if self.orientation == 'right':
            self.image = self.images[self.action][self.index]
        else:
            self.image = pygame.transform.flip(self.images[self.action][self.index], True, False)

        self.index += 1

