from typing import Optional, Tuple

import pygame
import random
from pygame.locals import *

from config import *


class Platform(pygame.sprite.Sprite):
    PLATFORM_HEIGHT = 26
    surf: pygame.Surface

    def __init__(self):
        super().__init__()

    def move(self):
        pass


class FloorPlatform(Platform):
    PLATFORM_HEIGHT = 26

    def __init__(self):
        super().__init__()
        self.surf = pygame.Surface((WIDTH, self.PLATFORM_HEIGHT))
        self.surf.set_colorkey((0, 0, 0))
        self.rect = self.surf.get_rect(center=(WIDTH / 2, HEIGHT - self.PLATFORM_HEIGHT / 2))

    def move(self):
        pass


class LevelPlatform(Platform):
    PLATFORM_HEIGHT = 26

    def __init__(self, width: Optional[int], position: Optional[Tuple[int, int]], transparent: Optional[bool]):
        super().__init__()
        if not width:
            width = random.randint(50, 120)  # random width

        if not position:
            position = (random.randint(0, WIDTH - 50), random.randint(0, HEIGHT - 30))  # random position

        self.surf = pygame.Surface((width, self.PLATFORM_HEIGHT))

        if transparent:
            self.surf.set_colorkey((0, 0, 0))
        else:
            self.surf.fill((0, 255, 0))
        self.rect: Rect = self.surf.get_rect(center=position)

    def move(self):
        pass
