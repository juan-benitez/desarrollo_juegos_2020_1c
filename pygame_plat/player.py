import glob
from typing import Tuple

import pygame
from pygame.math import Vector2

from config import *
from shot import Shot


class Player(pygame.sprite.Sprite):
    CHARACTER_WIDTH = 40
    CHARACTER_HEIGHT = 50
    CHARACTER_SIZE = (CHARACTER_WIDTH, CHARACTER_HEIGHT)

    controls: dict

    def __init__(self, game, avatar, controls: dict, starting_position: Tuple[int, int], start_orientation: str):
        super().__init__()
        self.game = game
        self.surf = pygame.Surface(self.CHARACTER_SIZE)
        self.rect = self.surf.get_rect(center=starting_position)
        self.controls = controls

        self.images = {
            'Idle': [pygame.transform.scale(pygame.image.load(image), self.CHARACTER_SIZE)
                     for image in glob.glob(f'img\\avatars\\{avatar}\\Idle*.png')],
            'Run': [pygame.transform.scale(pygame.image.load(image), self.CHARACTER_SIZE)
                    for image in glob.glob(f'img\\avatars\\{avatar}\\Run*.png')],
            'Jump': [pygame.transform.scale(pygame.image.load(image), self.CHARACTER_SIZE)
                     for image in glob.glob(f'img\\avatars\\{avatar}\\Jump*.png')],
            'Shoot': [pygame.transform.scale(pygame.image.load(image), self.CHARACTER_SIZE)
                     for image in glob.glob(f'img\\avatars\\{avatar}\\Shoot*.png')],
            'Dead': [pygame.transform.scale(pygame.image.load(image), self.CHARACTER_SIZE)
                     for image in glob.glob(f'img\\avatars\\{avatar}\\Dead*.png')]
        }

        self.action = 'Idle'
        self.orientation = start_orientation
        self.index = 0
        self.image = self.images[self.action][self.index]
        self.health = 5

        self.jumping = False
        self.pos = pygame.math.Vector2(starting_position)
        self.vel = pygame.math.Vector2((0, 0))
        self.acc = pygame.math.Vector2((0, 0))

    def move(self, keys):
        if not self.action == 'Dead':
            self.acc = pygame.math.Vector2((0, GRAVITY))

            idle = True

            if keys[self.controls['jump']]:
                idle = False
                self.jump()

            if keys[self.controls['move_left']]:
                self.acc.x = -ACC
                self.orientation = 'left'
                idle = False
                if not self.jumping:
                    self.set_action('Run')

            elif keys[self.controls['move_right']]:
                self.acc.x = ACC
                self.orientation = 'right'
                idle = False
                if not self.jumping:
                    self.set_action('Run')

            if keys[self.controls['shoot']]:
                idle = False
                self.set_action('Shoot')
                bullet_pos = Vector2(self.pos)

                if self.orientation == 'right':
                    bullet_pos.x = self.pos.x + 20
                else:
                    bullet_pos.x = self.pos.x - 20

                bullet_pos.y = self.pos.y - 19
                self.game.shot_sprites.add(Shot(self.game, "bala", bullet_pos, self.orientation))

            if idle:
                self.set_action('Idle')

            self.acc.x += self.vel.x * FRIC
            self.vel += self.acc
            self.pos += self.vel + 0.5 * self.acc

            if self.pos.x > WIDTH:
                self.pos.x = 0
            if self.pos.x < 0:
                self.pos.x = WIDTH

            self.rect.midbottom = self.pos

    def jump(self):
        hits = pygame.sprite.spritecollide(self, self.game.platforms, False)
        self.set_action('Jump')
        self.jumping = True
        if hits:  # Para evitar mas de un salto
            self.vel.y = JUMP_SPEED

    def set_action(self, action):
        if not self.action == action:
            self.index = 0
        if not self.action == 'Dead':
            self.action = action

    def take_damage(self, damage):
        self.health = self.health - damage
        if self.health <= 0:
            self.set_action('Dead')

    def update(self):
        if not self.action == 'Dead':
            if self.index >= len(self.images[self.action]):
                self.index = 0

            hits = pygame.sprite.spritecollide(self, self.game.platforms, False)
            if self.vel.y > 0:
                if hits:
                    self.jumping = False
                    if self.pos.y < hits[0].rect.bottom:
                        self.vel.y = 0
                        self.pos.y = hits[0].rect.top + 1

        else:
            if self.index >= len(self.images[self.action]):
                self.game.player_sprites.remove(self)
                self.index = len(self.images[self.action]) - 1
                self.game.end_game()

        if self.orientation == 'right':
            self.image = self.images[self.action][self.index]
        else:
            self.image = pygame.transform.flip(self.images[self.action][self.index], True, False)

        self.index += 1
