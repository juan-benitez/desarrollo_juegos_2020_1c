﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Game1
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch spriteBatch;
        private int score, maxScore;

        private Texture2D ball10;
        private Vector2 ballPos;
        private SpriteFont font;

        private Random random;

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            this.Content.RootDirectory = "Content";
            this.IsMouseVisible = true;
        }

        protected override void Initialize()
        {

            // TODO: Add your initialization logic here
            this.random = new Random();
            this.score = 0;
            this.maxScore = 0;
            this.ballPos = new Vector2(0, 0);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            

            // TODO: use this.Content to load your game content here
            ball10 = Content.Load<Texture2D>("10");
            font = Content.Load<SpriteFont>("textoNormal");
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                Keyboard.GetState().IsKeyDown(Keys.Escape))

                Exit();

            // TODO: Add your update logic here
            if ((long)gameTime.TotalGameTime.TotalSeconds % 5 == 0)
            {
                int x = random.Next(0, this._graphics.GraphicsDevice.Viewport.Width - 50);
                int y = random.Next(0, this._graphics.GraphicsDevice.Viewport.Height - 50);
                this.ballPos = new Vector2(x, y);
            }

            if (Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                var posMouse = Mouse.GetState().Position;
                var recMouse = new Rectangle(posMouse.X, posMouse.Y, 1, 1);

                var recBall = new Rectangle((int)ballPos.X, (int)ballPos.Y, 50, 50);

                if (recBall.Intersects(recMouse))
                {
                    this.score += 1;
                }
            }
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            spriteBatch.Draw(ball10, new Rectangle((int)this.ballPos.X, (int)this.ballPos.Y, 50, 50), Color.Red);
            //spriteBatch.Draw(ball10, this.ballPos, null, Color.White, 0, this.ballPos, .1f, SpriteEffects.None, 0);
            spriteBatch.DrawString(font, "Puntaje: " + this.score, new Vector2(0, 0), Color.Green);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
