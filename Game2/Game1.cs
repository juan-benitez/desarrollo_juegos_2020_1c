﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game2
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private ControllableCharacter sam, max;
        private ScrollableBackground background;

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = false;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            this.sam = new ControllableCharacter(this, Content.Load<Texture2D>("sam"), 7, new Vector2(0, 370));
            this.max = new ControllableCharacter(this, Content.Load<Texture2D>("sam"), 7, new Vector2(300, 370), Keys.D,Keys.A, Keys.W);

            this.background = new ScrollableBackground(this, Content.Load<Texture2D>("background"));

            this.sam.CharacterWalking += Sam_CharacterMoving;
            this.sam.CharacterStill += Sam_CharacterStill;
        }

        private void Sam_CharacterStill()
        {
            this.background.Speed = 0;
        }

        private void Sam_CharacterMoving(CharacterState state)
        {
            if (state == CharacterState.WALKING_RIGHT)
                this.background.Speed = 3;
            else if (state == CharacterState.WALKING_LEFT)
                this.background.Speed = -3;
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            this.background.Update(gameTime);
            this.sam.Update(gameTime);
            this.max.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin();

            background.Draw(gameTime, _spriteBatch);
            sam.Draw(gameTime, _spriteBatch);
            max.Draw(gameTime, _spriteBatch);

            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
