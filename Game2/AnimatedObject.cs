﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Game2
{
    public abstract class AnimatedObject : GameComponent
    {
        public delegate void OnCharacterWalking(CharacterState state);
        public delegate void OnCharacterStill();

        public event OnCharacterWalking CharacterWalking;
        public event OnCharacterStill CharacterStill;

        protected Texture2D texture;
        protected Vector2 position;
        protected CharacterState state = CharacterState.STILL_RIGHT;

        public AnimatedObject(Game game, Texture2D texture) : base(game)
        {
            this.texture = texture;
        }

        public void DoCharacterWalking(CharacterState state)
        {
            if (CharacterWalking != null)
                CharacterWalking.Invoke(state);
        }

        public void DoCharacterStill()
        {
            if (CharacterStill != null)
                CharacterStill.Invoke();
        }

        public abstract Rectangle GetTextureRentagle();

        public void Draw(GameTime gameTime, SpriteBatch sb)
        {
            if (this.state == CharacterState.WALKING_RIGHT || this.state == CharacterState.STILL_RIGHT)
                sb.Draw(this.texture, this.position, this.GetTextureRentagle(), Color.White, 0f, Vector2.Zero, 1, SpriteEffects.None, 0);
            else if (this.state == CharacterState.WALKING_LEFT || this.state == CharacterState.STILL_LEFT)
                sb.Draw(this.texture, this.position, this.GetTextureRentagle(), Color.White, 0f, Vector2.Zero, 1, SpriteEffects.FlipHorizontally, 0);
        }
    }
}
