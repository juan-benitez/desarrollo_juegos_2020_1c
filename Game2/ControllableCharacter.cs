﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;

namespace Game2
{
    class ControllableCharacter : AnimatedObject
    {
        private int frameNumber;
        private int totalFrames;
        private double lastUpdate;
        private int frameWidth, frameHeight;
        private Keys leftKey, rightKey, jumpKey;

        public ControllableCharacter(Game game, Texture2D texture, int totalFrames, Vector2 position,
                                     Keys rightKey = Keys.Right, Keys leftKey = Keys.Left, Keys jumpKey = Keys.Up) 
            : base(game, texture)
        {
            this.totalFrames = totalFrames;
            this.position = position;
            this.frameHeight = this.texture.Height;
            this.frameWidth = this.texture.Width / this.totalFrames;
            this.leftKey = leftKey;
            this.rightKey = rightKey;
            this.jumpKey = jumpKey;
        }

        public override void Update(GameTime gameTime)
        {
            var keyState = Keyboard.GetState();

            if (keyState.IsKeyDown(rightKey)) {
                this.state = CharacterState.WALKING_RIGHT;

            } else if (keyState.IsKeyDown(leftKey))
            {
                this.state = CharacterState.WALKING_LEFT;
            } else
            {
                switch (this.state)
                {
                    case CharacterState.WALKING_RIGHT:
                        this.state = CharacterState.STILL_RIGHT;
                        this.frameNumber = 0;
                        break;

                    case CharacterState.WALKING_LEFT:
                        this.state = CharacterState.STILL_LEFT;
                        this.frameNumber = 0;
                        break;
                }
            }

            switch (state)
            {
                case CharacterState.WALKING_RIGHT:
                case CharacterState.WALKING_LEFT:
                    this.DoCharacterWalking(state);
                    break;

                case CharacterState.STILL_RIGHT:
                case CharacterState.STILL_LEFT:
                    this.DoCharacterStill();
                    break;

                case CharacterState.JUMPING_LEFT:
                case CharacterState.JUMPING_RIGHT:
                case CharacterState.JUMPING_UP:
                    break;
            }

            if ((gameTime.TotalGameTime.TotalMilliseconds - lastUpdate > 100) &&
                (state != CharacterState.STILL_LEFT && state != CharacterState.STILL_RIGHT))
            {
                this.frameNumber = (frameNumber + 1) % this.totalFrames;
                this.lastUpdate = gameTime.TotalGameTime.TotalMilliseconds;
            }

            base.Update(gameTime);
        }

        public override Rectangle GetTextureRentagle()
        {
            return new Rectangle(this.frameWidth * this.frameNumber, 0, this.frameWidth, this.frameHeight);
        }
    }
}
