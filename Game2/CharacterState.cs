﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Game2
{
    public enum CharacterState
    {
        WALKING_RIGHT,
        WALKING_LEFT,
        STILL_RIGHT,
        STILL_LEFT,
        JUMPING_LEFT,
        JUMPING_RIGHT,
        JUMPING_UP,
    }
}
